ARG BASE_IMG='ubuntu:20.04'

FROM ${BASE_IMG}
LABEL org.opencontainers.image.authors="Vvlad1slavV@yandex.ru"

EXPOSE 11311
SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive
ENV GUI false
WORKDIR '/ros_ws'

# Timezone Configuration
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install dependencies for ROS
RUN apt-get update && apt-get upgrade -y &&\
    apt-get install --no-install-recommends -y \
    build-essential cmake gcc g++ git curl wget unzip \
    python3-dev python3-pip \
    vim nano \
    && rm -rf /var/lib/apt/lists/*
